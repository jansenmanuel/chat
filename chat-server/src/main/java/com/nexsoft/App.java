package com.nexsoft;


import com.nexsoft.Chat.ServletChat;
import com.nexsoft.Chat.Message.ServletChatMessage;
import com.nexsoft.Chat.LoadMessage.ServletLoadMessage;
import com.nexsoft.Login.ServletLogin;
import com.nexsoft.Register.ServletRegister;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;

public class App 
{
    public static void main( String[] args )
    {
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8080);
        server.addConnector(connector);


        ServletContextHandler handler = new ServletContextHandler(server, "/");
        handler.addServlet(ServletRegister.class, "/register");
        handler.addServlet(ServletLogin.class, "/login");
        handler.addServlet(ServletChat.class, "/userChat");
        handler.addServlet(ServletChatMessage.class, "/messageChat");
        handler.addServlet(ServletLoadMessage.class, "/loadChat");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
