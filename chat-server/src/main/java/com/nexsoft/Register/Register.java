package com.nexsoft.Register;


import java.io.IOException;
import java.lang.reflect.Array;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.nexsoft.connection.MySQLConn;
import com.nexsoft.data.Account;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

@WebSocket
public class Register {
    Account account = new Account();
    MySQLConn mySQLConn = new MySQLConn();
    boolean isRegister = false;
    protected Session session = null;

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        try {
            this.session = session;
            session.getRemote().sendString("Hello Web browser from class Register");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @OnWebSocketMessage
    public void onMessage(String message) throws NullPointerException{
        try {
            // convert string to json array
            JsonArray convertedObject = new Gson().fromJson(message, JsonArray.class);
            // get element from json array and get as string data type
            String username = convertedObject.get(0).getAsString();
            String password = convertedObject.get(1).getAsString();
            register(message, username, password);
            if (isRegister) {
                this.session.getRemote().sendString("berhasil");
                this.session.close();
            }
        } catch (NullPointerException | IOException e) {
            e.getMessage();
        }

    }

    public void register(String message, String username, String password) {
        if (!message.equals("")) {
            account.setUsername(username);
            account.setPasword(password);
            if (!account.getUsername().equals("") && !account.getPasword().equals("")) {
                mySQLConn.register(account.getUsername(), account.getPasword());
                isRegister = true;

            }
        }
    }
}