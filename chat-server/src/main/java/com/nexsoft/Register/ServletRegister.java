package com.nexsoft.Register;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class ServletRegister extends WebSocketServlet
{
    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.register(Register.class);
    }
}