package com.nexsoft.connection;

import java.sql.*;

public class MySQLConn {
    public void register(String username, String password) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            String query = "INSERT INTO account (username, password)" + " VALUES (?, ?)";

            if (!username.equals("") && !password.equals("")) {
                PreparedStatement preparedStatement = myConn.prepareStatement(query);
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, password);
                preparedStatement.execute();
            }
            createTableMessage(username);
            myConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean login(String username, String password) {
        boolean isLogin = false;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            String query = "SELECT * FROM account WHERE username='"+username+"' AND password='"+password+"'";

            if (!username.equals("") && !password.equals("")) {
                ResultSet rs = statement.executeQuery(query);
                while (rs.next()) {
                    String usernameData = rs.getString("username");
                    String passwordData = rs.getString("password");
                    System.out.println(usernameData);
                    System.out.println(passwordData);
                    isLogin = true;
                }
            }
            myConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isLogin;
    }

    public String[] getContact() {
        int i = 0;
        String[] contacts = new String[getUserCount()];
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            String query = "SELECT * FROM account";
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                String usernameData = rs.getString("username");
                contacts[i] = usernameData;
                i++;
            }
            myConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return contacts;
    }

    public void addMessage(String username, String message, String toUser) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            String query = "INSERT INTO "+ toUser +" (username, message)" + " VALUES (?, ?)";

            if (!username.equals("") && !message.equals("")) {
                PreparedStatement preparedStatement = myConn.prepareStatement(query);
                preparedStatement.setString(1, username);
                preparedStatement.setString(2, message);
                preparedStatement.execute();
            }
            myConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String[] getMessage(String fromUser, String toUser) {
        String[] message = new String[getMessageCount(fromUser, toUser)];
        int i = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            System.out.println(toUser);
            System.out.println(fromUser);
            String query = "SELECT message, time, username FROM " + fromUser + " WHERE username= '" + toUser + "' UNION SELECT message, time, username FROM " + toUser + " WHERE username='" + fromUser + "' ORDER BY time";

            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                String messageData = rs.getString("message");
                String usernamaData = rs.getString("username");
                message[i] = usernamaData + "-" + messageData  ;
                i++;
            }
            myConn.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return message;
    }

    public static int getMessageCount(String fromUser, String toUser)  {
        int row = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();
            System.out.println(toUser);
            System.out.println(fromUser);
            String query = "SELECT SUM(row.v) FROM (SELECT COUNT(*) AS v FROM " + fromUser + " UNION ALL SELECT COUNT(*) AS v FROM " + toUser +  " WHERE username= '" + fromUser + "') row";

            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                row = rs.getInt(1);
                System.out.println(row);
            }
            myConn.close();
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        return row;
    }


    // for count all users in table account
    public static int getUserCount() {
        int row = 0;
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            String query = "SELECT COUNT(*) FROM account";
            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                row = rs.getInt(1);
            }
            myConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return row;
    }


    public static void createTableMessage(String username) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/chat?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            String query = "CREATE TABLE " + username+ " (id int(4) AUTO_INCREMENT, username varchar(50), message varchar(50), time TIMESTAMP DEFAULT CURRENT_TIMESTAMP, PRIMARY KEY (id))";

            statement.executeUpdate(query);
            myConn.close();
        } catch (Exception e) {
            e.getMessage();
        }
    }
}