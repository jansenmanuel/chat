package com.nexsoft.Login;


import java.io.IOException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.nexsoft.connection.MySQLConn;
import com.nexsoft.data.Account;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import javax.validation.constraints.NotNull;

@WebSocket
public class Login {
    private Session session;
    Account account = new Account();
    MySQLConn mySQLConn = new MySQLConn();
    boolean isLogin = false;

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        try {
            session.getRemote().sendString("Hello Client from Class Login");
        } catch (IOException | NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

    @OnWebSocketMessage
    public void onMessage(String message) throws NullPointerException {
        try {
            JsonArray convertedObject = new Gson().fromJson(message, JsonArray.class);
            String username = convertedObject.get(0).getAsString();
            String password = convertedObject.get(1).getAsString();
            login(message, username, password);
            if (isLogin) {
                this.session.getRemote().sendString("berhasil");
                this.session.close();
            }
        } catch (NullPointerException | IOException e) {
            e.getMessage();
        }
    }

    public void login(String message, String username, String password) {
        if (!message.equals("")) {
            account.setUsername(username);
            account.setPasword(password);
            if (!account.getUsername().equals("") && !account.getPasword().equals("")) {
                this.isLogin = mySQLConn.login(account.getUsername(), account.getPasword());
                System.out.println(mySQLConn.login(account.getUsername(), account.getPasword()));
            }
        }
    }
}