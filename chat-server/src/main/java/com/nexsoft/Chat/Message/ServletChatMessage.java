package com.nexsoft.Chat.Message;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class ServletChatMessage extends WebSocketServlet
{
    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.register(ChatMessage.class);
    }
}