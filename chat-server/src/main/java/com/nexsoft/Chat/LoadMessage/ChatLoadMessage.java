package com.nexsoft.Chat.LoadMessage;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.nexsoft.connection.MySQLConn;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;


@WebSocket
public class ChatLoadMessage {
    private Session session;
    MySQLConn mySQLConn = new MySQLConn();

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        try {
            session.getRemote().sendString("");
         } catch (IOException | NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

    @OnWebSocketMessage
    public void onMessage(String message)  {
        try {
            JsonArray convertedObject = new Gson().fromJson(message, JsonArray.class);
            String fromUser = convertedObject.get(0).getAsString();
            String toUser = convertedObject.get(1).getAsString();

            for (String msg : mySQLConn.getMessage(fromUser, toUser)) {
                this.session.getRemote().sendString(msg);
            }
            this.session.close();
        } catch (NullPointerException | IOException e) {
            System.out.println(e.getMessage());
        }
    }
}