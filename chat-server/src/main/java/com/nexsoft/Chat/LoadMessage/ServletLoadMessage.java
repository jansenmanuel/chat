package com.nexsoft.Chat.LoadMessage;

import com.nexsoft.Chat.LoadMessage.ChatLoadMessage;
import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class ServletLoadMessage extends WebSocketServlet
{
    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.register(ChatLoadMessage.class);
    }
}