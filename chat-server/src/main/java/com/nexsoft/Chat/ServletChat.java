package com.nexsoft.Chat;

import org.eclipse.jetty.websocket.servlet.WebSocketServlet;
import org.eclipse.jetty.websocket.servlet.WebSocketServletFactory;

public class ServletChat extends WebSocketServlet
{
    @Override
    public void configure(WebSocketServletFactory factory) {
        factory.register(ChatUser.class);
    }
}