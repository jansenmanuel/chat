package com.nexsoft.Chat;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.nexsoft.connection.MySQLConn;
import com.nexsoft.data.Account;
import com.nexsoft.data.Message;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketError;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;

import java.io.IOException;


@WebSocket
public class ChatUser {
    private Session session;
    MySQLConn mySQLConn = new MySQLConn();

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        try {
            for (String username : mySQLConn.getContact()) {
                session.getRemote().sendString(username);
            }
        } catch (IOException | NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }
}