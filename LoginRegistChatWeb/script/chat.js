const accounts = [];
let message = [];
const userMessage = [];
let toUser = "";

function loadUser() {
    const chatSocket = new WebSocket("ws://10.10.1.179:8080/userChat");
    
    chatSocket.addEventListener("open", function (event) {
        console.log("Connect to server chat");
    });

    chatSocket.addEventListener("message", function (event) {
        accounts.push(event.data);
        showUser();
    });
}

function showUser() {
    if (accounts.length > 1) {
        const listAccount = document.querySelector("#contact");
        const username = getUrlVars()["username"];
        listAccount.innerHTML = "";
        for (let i = 0; i < accounts.length; i++) {
            if (accounts[i] !== username) {
                listAccount.innerHTML += `
                <li>
                    <div class="user_info">
                        <span id="${accounts[i]}" onclick="changeUser(id)">${accounts[i]}</span>
                    </div>
                </li>`;    
            }
        }
    }
}

function sendMessage() {
        const chatSocketMessage = new WebSocket("ws://10.10.1.179:8080/messageChat");
        const messageValue = document.querySelector("#messageBox").value;
        const chat = JSON.stringify([getUrlVars()["username"], messageValue, toUser]); 
        
        chatSocketMessage.addEventListener("open", function (event) {
            console.log("Connect to server message");
        });

        chatSocketMessage.addEventListener("message", function (event) {
            chatSocketMessage.send(chat);
            console.log(chat)
            message = [];
            loadMessage();
            deleteValue();
         });         
}
 
function changeUser(user) {
    toUser = user; 
    message = [];
    loadMessage();
}

function loadMessage() {
    if (accounts.length > 1) {
        const chatSocketGetMessage = new WebSocket("ws://10.10.1.179:8080/loadChat");     
        const chat = JSON.stringify([getUrlVars()["username"], toUser]); 
    
        chatSocketGetMessage.addEventListener("open", function (event) {
            console.log("Connect to server chat");
        });

        chatSocketGetMessage.addEventListener("message", function (event) {
            chatSocketGetMessage.send(chat);     
             if (event.data != "") {
                message.push({user: toUser, message: event.data});
                showMessage();
            }    
        });
    }
}


function showMessage() {
    const listMessage = document.querySelector("#bubble-msg")
    listMessage.innerHTML = "";
   if (message.length >= 1) {
    let item = "";
        for (let i = 0; i < message.length; i++) {
            const msg = message[i].message;
            const user = message[i].user;
            if (user == toUser) {
                const res = msg.split("-");
                if (res[0] == toUser) {
                    const res = msg.split("-");
                    item = `<div class="msg_cotainer2">${res[1]}</div>`;
                } else {
                    const res = msg.split("-");
                    item = `<div class="msg_cotainer">${res[1]}</div>`;
                }                
                listMessage.innerHTML += item;;           
            }  
        }
    } else {
        listMessage.innerHTML = "";
    }
}

function deleteValue() {  //instead of "delete"
    const messageValue = document.querySelector("#messageBox");
    messageValue.value = "";
}
 
function getUrlVars() {
    const vars = {};
    const parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}

loadUser();
document.getElementById("chatLogin").textContent = `Login Sebagai, ${getUrlVars()["username"]}`;
